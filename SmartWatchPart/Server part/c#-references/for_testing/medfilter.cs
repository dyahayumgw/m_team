/*
* MATLAB Compiler: 6.1 (R2015b)
* Date: Sun Dec 24 13:26:15 2017
* Arguments: "-B" "macro_default" "-W" "dotnet:MedFilter,medfilter,0.0,private" "-T"
* "link:lib" "-d" "D:\Driver Team\matlab\medianfiltertest\for_testing" "-v"
* "class{medfilter:D:\Driver Team\matlab\setyan\medianfiltertest.m}" 
*/
using System;
using System.Reflection;
using System.IO;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;

#if SHARED
[assembly: System.Reflection.AssemblyKeyFile(@"")]
#endif

namespace MedFilter
{

  /// <summary>
  /// The medfilter class provides a CLS compliant, MWArray interface to the MATLAB
  /// functions contained in the files:
  /// <newpara></newpara>
  /// D:\Driver Team\matlab\setyan\medianfiltertest.m
  /// </summary>
  /// <remarks>
  /// @Version 0.0
  /// </remarks>
  public class medfilter : IDisposable
  {
    #region Constructors

    /// <summary internal= "true">
    /// The static constructor instantiates and initializes the MATLAB Runtime instance.
    /// </summary>
    static medfilter()
    {
      if (MWMCR.MCRAppInitialized)
      {
        try
        {
          Assembly assembly= Assembly.GetExecutingAssembly();

          string ctfFilePath= assembly.Location;

          int lastDelimiter= ctfFilePath.LastIndexOf(@"\");

          ctfFilePath= ctfFilePath.Remove(lastDelimiter, (ctfFilePath.Length - lastDelimiter));

          string ctfFileName = "MedFilter.ctf";

          Stream embeddedCtfStream = null;

          String[] resourceStrings = assembly.GetManifestResourceNames();

          foreach (String name in resourceStrings)
          {
            if (name.Contains(ctfFileName))
            {
              embeddedCtfStream = assembly.GetManifestResourceStream(name);
              break;
            }
          }
          mcr= new MWMCR("",
                         ctfFilePath, embeddedCtfStream, true);
        }
        catch(Exception ex)
        {
          ex_ = new Exception("MWArray assembly failed to be initialized", ex);
        }
      }
      else
      {
        ex_ = new ApplicationException("MWArray assembly could not be initialized");
      }
    }


    /// <summary>
    /// Constructs a new instance of the medfilter class.
    /// </summary>
    public medfilter()
    {
      if(ex_ != null)
      {
        throw ex_;
      }
    }


    #endregion Constructors

    #region Finalize

    /// <summary internal= "true">
    /// Class destructor called by the CLR garbage collector.
    /// </summary>
    ~medfilter()
    {
      Dispose(false);
    }


    /// <summary>
    /// Frees the native resources associated with this object
    /// </summary>
    public void Dispose()
    {
      Dispose(true);

      GC.SuppressFinalize(this);
    }


    /// <summary internal= "true">
    /// Internal dispose function
    /// </summary>
    protected virtual void Dispose(bool disposing)
    {
      if (!disposed)
      {
        disposed= true;

        if (disposing)
        {
          // Free managed resources;
        }

        // Free native resources
      }
    }


    #endregion Finalize

    #region Methods

    /// <summary>
    /// Provides a single output, 0-input MWArrayinterface to the medianfiltertest MATLAB
    /// function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// MEDIANFILTER Summary of this function goes here
    /// Detailed explanation goes here
    /// </remarks>
    /// <returns>An MWArray containing the first output argument.</returns>
    ///
    public MWArray medianfiltertest()
    {
      return mcr.EvaluateFunction("medianfiltertest", new MWArray[]{});
    }


    /// <summary>
    /// Provides a single output, 1-input MWArrayinterface to the medianfiltertest MATLAB
    /// function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// MEDIANFILTER Summary of this function goes here
    /// Detailed explanation goes here
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <returns>An MWArray containing the first output argument.</returns>
    ///
    public MWArray medianfiltertest(MWArray data)
    {
      return mcr.EvaluateFunction("medianfiltertest", data);
    }


    /// <summary>
    /// Provides a single output, 2-input MWArrayinterface to the medianfiltertest MATLAB
    /// function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// MEDIANFILTER Summary of this function goes here
    /// Detailed explanation goes here
    /// </remarks>
    /// <param name="data">Input argument #1</param>
    /// <param name="num1">Input argument #2</param>
    /// <returns>An MWArray containing the first output argument.</returns>
    ///
    public MWArray medianfiltertest(MWArray data, MWArray num1)
    {
      return mcr.EvaluateFunction("medianfiltertest", data, num1);
    }


    /// <summary>
    /// Provides the standard 0-input MWArray interface to the medianfiltertest MATLAB
    /// function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// MEDIANFILTER Summary of this function goes here
    /// Detailed explanation goes here
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public MWArray[] medianfiltertest(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "medianfiltertest", new MWArray[]{});
    }


    /// <summary>
    /// Provides the standard 1-input MWArray interface to the medianfiltertest MATLAB
    /// function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// MEDIANFILTER Summary of this function goes here
    /// Detailed explanation goes here
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public MWArray[] medianfiltertest(int numArgsOut, MWArray data)
    {
      return mcr.EvaluateFunction(numArgsOut, "medianfiltertest", data);
    }


    /// <summary>
    /// Provides the standard 2-input MWArray interface to the medianfiltertest MATLAB
    /// function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// MEDIANFILTER Summary of this function goes here
    /// Detailed explanation goes here
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="data">Input argument #1</param>
    /// <param name="num1">Input argument #2</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public MWArray[] medianfiltertest(int numArgsOut, MWArray data, MWArray num1)
    {
      return mcr.EvaluateFunction(numArgsOut, "medianfiltertest", data, num1);
    }


    /// <summary>
    /// Provides an interface for the medianfiltertest function in which the input and
    /// output
    /// arguments are specified as an array of MWArrays.
    /// </summary>
    /// <remarks>
    /// This method will allocate and return by reference the output argument
    /// array.<newpara></newpara>
    /// M-Documentation:
    /// MEDIANFILTER Summary of this function goes here
    /// Detailed explanation goes here
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return</param>
    /// <param name= "argsOut">Array of MWArray output arguments</param>
    /// <param name= "argsIn">Array of MWArray input arguments</param>
    ///
    public void medianfiltertest(int numArgsOut, ref MWArray[] argsOut, MWArray[] argsIn)
    {
      mcr.EvaluateFunction("medianfiltertest", numArgsOut, ref argsOut, argsIn);
    }



    /// <summary>
    /// This method will cause a MATLAB figure window to behave as a modal dialog box.
    /// The method will not return until all the figure windows associated with this
    /// component have been closed.
    /// </summary>
    /// <remarks>
    /// An application should only call this method when required to keep the
    /// MATLAB figure window from disappearing.  Other techniques, such as calling
    /// Console.ReadLine() from the application should be considered where
    /// possible.</remarks>
    ///
    public void WaitForFiguresToDie()
    {
      mcr.WaitForFiguresToDie();
    }



    #endregion Methods

    #region Class Members

    private static MWMCR mcr= null;

    private static Exception ex_= null;

    private bool disposed= false;

    #endregion Class Members
  }
}
